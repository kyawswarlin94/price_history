<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Market;
use Faker\Generator as Faker;

$factory->define(Market::class, function (Faker $faker) {
	$user_id = App\User::all()->random()->id;
	return [
		'name' => $faker->company,
		'created_by' => $user_id,
	];
});
