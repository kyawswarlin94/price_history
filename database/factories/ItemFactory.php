<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Item;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Item::class, function (Faker $faker) {
	return [
		'name' => $faker->word,
		'sku' => Str::random(10),
		'qty' => $faker->randomElement($array = array (1, 10, 100)),
		'unit' => $faker->randomElement($array = array ('g', 'kg', 'unit')),
		'created_by' => \App\User::all()->random()->id,
	];
});
