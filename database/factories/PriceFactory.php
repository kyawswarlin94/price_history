<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Price;
use Faker\Generator as Faker;

$factory->define(Price::class, function (Faker $faker) {
    $_id = \App\Item::all()->random()->id;
    $market_id = \App\Market::all()->random()->id;

    return [
        "item_id" => \App\Item::all()->random()->id,
        "price" => $faker->randomNumber(4),
        "market_id" =>  \App\Market::all()->random()->id,
        "created_by" =>  \App\User::all()->random()->id,
        "created_at" => $faker->dateTime(),
    ];
});
