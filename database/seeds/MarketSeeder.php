<?php

namespace Database\Seeds;

use App\Market;
use Illuminate\Database\Seeder;

class MarketSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory(Market::class)->create([
			'name' => 'Aung Ban',
			'created_by' => 1,
		]);

		factory(Market::class)->create([
			'name' => 'Thiri Mingalar',
			'created_by' => 1,
		]);
	}
}
