<?php

namespace Database\Seeds;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory(User::class)->create([
			'name' => 'Ba Din',
			'email' => 'badin@example.com',
			'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
			'api_token' => Str::random(80),
			'remember_token' => Str::random(10),
		]);
	}
}
