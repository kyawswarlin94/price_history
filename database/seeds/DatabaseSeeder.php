<?php

use Database\Seeds\ItemSeeder;
use Database\Seeds\MarketSeeder;
use Database\Seeds\UserSeeder;
use Illuminate\Database\Seeder;
use App\Price;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {
		$this->call(UserSeeder::class);
		$this->call(MarketSeeder::class);
		$this->call(ItemSeeder::class);
		factory(Price::class, 50)->create();
	}
}
