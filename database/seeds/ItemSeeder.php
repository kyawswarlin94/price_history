<?php

namespace Database\Seeds;

use App\Item;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ItemSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		factory(Item::class)->create([
			'name' => 'paddy',
			'sku' => Str::random(8),
			'created_by' => 1,
			'qty' => 1,
			'unit'=> 'g'
		]);
		factory(Item::class)->create([
			'name' => 'mungbean',
			'sku' => Str::random(8),
			'created_by' => 1,
			'qty' => 1,
			'unit'=> 'g'
		]);
		factory(Item::class)->create([
			'name' => 'watermelons',
			'sku' => Str::random(8),
			'created_by' => 1,
			'qty' => 1,
			'unit'=> 'g'
		]);
	}
}
