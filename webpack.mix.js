const mix = require("laravel-mix");

mix
  .js("resources/js/app.js", "public/js")
  .sass("resources/sass/app.scss", "public/css")
  .webpackConfig({
    module: {
      rules: [
        {
            test: /\.tsx?$/,
            loader: 'ts-loader',
            options: { appendTsSuffixTo: [/\.vue$/] },
            exclude: /node_modules/,
        }
      ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"]
      }
  });