<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $fillable = [
        'price', 'item_id', 'market_id', 'created_by'
    ];

    public function item(){
        return $this->belongsTo('App\Item');
    }

    public function market(){
        return $this->belongsTo('App\Market');
    }
}
