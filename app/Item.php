<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Item extends Model {
	protected $fillable = [
		'id', 'name', 'sku', 'qty', 'unit', 'created_by'
	];


	public function prices() {
		return $this->hasMany(Price::class);
	}

	public static function listItems($page = 0, $market_id = 'null', $item_id = 'null'){
		$limit = 100;
		$offset = $page > 0 ? ($page - 1) * 100 : 0;
		$sql = <<<SQL
			SELECT
				t1.name
				,t1.sku
				,t3.name as market_name
				,t1.qty
				,t1.unit
				,t2.price
				,t2.created_at as price_date_at
			FROM
				items AS t1
			INNER JOIN prices AS t2 ON t1.id = t2.item_id
			INNER JOIN markets AS t3 ON t2.market_id = t3.id
			WHERE
				($market_id IS NULL OR t2.market_id=$market_id)
				AND ($item_id IS NULL OR t2.item_id=$item_id)
			ORDER BY
				t2.created_at desc,
				t1.id desc 
		SQL;
		$limit_offset = $page>0 ? "LIMIT $limit OFSET $offset" : ''; 
        return DB::select("$sql $limit_offset");
	}

	public static function onlyLatestPrice($page = 0, $market_id = 'null', $item_id = 'null'){
		$limit = 100;
        $offset = $page>0 ? ($page - 1) * 100 : 0; 
        $sql = <<<SQL
            SELECT
                t12.name
                ,t12.sku
                ,t13.name as market_name
                ,t12.qty
                ,t12.unit
                ,t11.price
                ,t11.created_at as price_date_at
            FROM (
                SELECT
                    t1.item_id
                    ,t1.market_id
                    ,max(t1.created_at) as created_at
                FROM
                    prices as t1
                group by
                    t1.item_id
                    ,t1.market_id
            ) AS t10
            INNER JOIN prices AS t11 ON
                t10.item_id = t11.item_id 
                AND t10.market_id = t11.market_id
                AND t10.created_at = t11.created_at
            INNER JOIN items AS t12 ON t10.item_id = t12.id
            INNER JOIN markets AS t13 ON t10.market_id = t13.id
            WHERE
                ($market_id IS NULL OR t11.market_id=$market_id)
                AND ($item_id IS NULL OR t11.item_id=$item_id )
        SQL;
		$limit_offset = $page>0 ? "LIMIT $limit OFSET $offset" : ''; 
		return DB::select("$sql $limit_offset");
    }
}
