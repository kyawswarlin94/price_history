<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model {
	protected $fillable = [
		'name', 'created_by',
	];

	public function items() {
		return $this->belongsToMany(Item::class);
	}
}
