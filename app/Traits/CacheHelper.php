<?php

namespace App\Traits;

/**
 * 
 */
trait CacheHelper
{
    public function requeutKeyGen($params){
        $key = config('cache.prefix');
        if(!count($params)){
            return '/';
        }

        foreach($params as $k => $v){
            $key = "$key|$k:$v";
        }

        return $key;
    }
}
