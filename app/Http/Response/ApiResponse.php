<?php

namespace App\Http\Response;

// 404	Not Found (page or other resource doesn’t exist)
// 401	Not authorized (not logged in)
// 403	Logged in but access to requested area is forbidden
// 400	Bad request (something wrong with URL or parameters)
// 422	Unprocessable Entity (validation failed)
// 500	General server error
class ApiResponse
{
    static function success($result){
        if(isset($result) && is_object($result)){
            return SELF::make(200, $result);
        } 
        else if(isset($result) && is_array($result)){
            return SELF::make(200, $result);
        }
        else {
            return SELF::make(200, []);
        }
    }
    
    static function badRequest(){
        return SELF::make(400, null,"Bad request!");
    }

    static function unauthenticate(){
        return SELF::make(401, null,"Unauthiticated!");
    }

    static function noPermission(){
        return SELF::make(403, null,"No Permission!");
    }
    
    static function notFound($message = null)
    {
        return SELF::make(404, null, $message ? $message : "Not Found!");
    }

    static function invalid($error = null){
        return SELF::make(422, null, $error !== null ? $error : "The given data was invalid.");
    }

    static function error($result)
    {
        return SELF::make(500, $result, 'General server error');
    }

    static function make($status, $result = null, $message = null)
    {
        $response = ["status" => $status];
        if($result !== null ) $response["result"] = $result;
        if($message !== null ) $response["messages"] = $message;
        return response()->json($response, $status);
    }
}
