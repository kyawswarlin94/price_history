<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Item\StoreRequest;
use App\Http\Requests\Item\ListRequest;
use Illuminate\Http\Request;
use App\Http\Response\ApiResponse;
use App\Item;
use App\Market;
use App\Price;
use Illuminate\Support\Facades\Cache;
use App\Traits\CacheHelper;

class ItemsController extends Controller
{   
    use CacheHelper;
/**
 * @api {GET} api/items Get Item List
 * @apiVersion 0.0.1
 * @apiName GetItem
 * @apiGroup Item
 * @apiPermission guest
 * 
 * @apiHeader {String} Content-Type=application/json
 * @apiHeader {String} Accept=application/json
 * @apiHeader {String} Authorization="Bearer {{acces token}}"
 *
 * 
 * @apiSampleRequest http://localhost:8000/api/items
 * 
 * @apiSuccess {Objest[]} $result
 * @apiSuccess {String} $result.name Name of the item.
 * @apiSuccess {String} $result.sku SKU of the item
 * @apiSuccess {String} $result.qty Quantity of minimum selling price
 * @apiSuccess {String} $result.unit Unit of item
 * @apiSuccess {Number} $result.created_by  ID of the User.
 * @apiSuccess {Date} $resilt.created_at  Created date of item.
 */
    public function index(Request $request)
    {
        $items = Item::orderBy('created_at', 'desc')->get();
        return ApiResponse::success($items);
    }

/**
 * @api {GET} api/item/list Get Item price list
 * @apiVersion 0.0.1
 * @apiName Item list with the data of market prices
 * @apiGroup Item
 * @apiPermission guest
 * 
 * @apiHeader {String} Content-Type=application/json
 * @apiHeader {String} Accept=application/json
 * @apiHeader {String} Authorization="Bearer {{acces token}}"
 * 
 * @apiParam {Boolean} [latest_price] To show only latest price list of items
 * @apiParam {Numeric} [market_id] To search with market id
 * @apiParam {Numeric} [item_id] To searchwith item id
 * 
 * @apiSampleRequest http://localhost:8000/api/item/list
 * 
 * @apiSuccess {Objest[]} $result
 * @apiSuccess {String} $result.name Name of the item.
 * @apiSuccess {String} $result.sku SKU of the item
 * @apiSuccess {Numeric} $result.qty Quantity of minimum selling price
 * @apiSuccess {String} $result.unit Unit of item
 * @apiSuccess {Numeric} $result.price Price of Item
 * @apiSuccess {String} $result.market_name Selling market place with  current price
 * @apiSuccess {Date} $resilt.price_date_at  Created date of price.
 * 
 * @apiError 422:InvalidParamerter The <code>item_id</code> must be id number.
 */ 
    public function list(ListRequest $request){
        $params = $request->validated();
        $cacheKey = $this->requeutKeyGen($params);

        if (Cache::has($cacheKey)){
            return ApiResponse::success(Cache::get($cacheKey));
        } else {
            $market_id = isset($params['market_id']) ? $params["market_id"] : 'null';
            $item_id = isset($params['item_id']) ? $params["item_id"] : 'null';

            if(isset($params['latest_price']) && $params['latest_price'] == 'true'){
                // TODO: Pagination - currently page number is 0
                $result = Item::onlyLatestPrice(0, $market_id, $item_id);
            } else {
                // TODO: Pagination - currently page number is 0
                $result = Item::listItems(0, $market_id, $item_id);
            }
            Cache::put($cacheKey, $result, 3);
            return ApiResponse::success($result);
        }
    }


/**
 * @api {POST} api/items Create new Item
 * @apiVersion 0.0.1
 * @apiName Create item and price
 * @apiGroup Item
 * @apiPermission auth:api
 * 
 * @apiHeader {String} Content-Type=application/json
 * @apiHeader {String} Accept=application/json
 * @apiHeader {String} Authorization="Bearer {{acces token}}"
 * 
 * @apiParam {String} item_name The name of item
 * @apiParam {String} sku SKU number
 * @apiParam {Numeric} qty Minimum selling amunt
 * @apiParam {String} [unit] The Unit of item. Default `Unit`
 * @apiParam {Numeric} price Selling Price
 * @apiParam {String} market_name The market place of selling the item.
 * 
 * @apiSampleRequest http://localhost:8000/api/items
 * 
 * @apiSuccess {Objest[]} $result
 * @apiSuccess {Numeric} $result.id Item id.
 * @apiSuccess {String} $result.name Name of the item.
 * @apiSuccess {String} $result.sku SKU of the item
 * @apiSuccess {Numeric} $result.qty Quantity of minimum selling price
 * @apiSuccess {String} $result.unit Unit of item
 * @apiSuccess {Numeric} $resilt.created_by  Created user id.
 * @apiSuccess {Date} $resilt.created_at  Created date of price.
 * 
 * @apiError 422:InvalidParamerter The <code>item_id</code> must be id number.
 */ 
    public function store(StoreRequest $request)
    {
        $params = $request->validated();

        $item = Item::where('sku', $params['sku'])->first();
        if(!$item) {
            $item = new Item([
                "name" => $params['item_name'],
                "sku" => $params['sku'],
                "qty" => $params['qty'],
                "unit" => $params['unit'] | null,
                "created_by" => $request->user()->id
            ]);
            $item->save();
        } else {
            if($item->name !== $params['item_name']){
                return response()->json([
                    "status"=> 422,
                    "messages"=> ['SKU already exist!']
                ]);
            }
        }
        $market = Market::where('name', $params['market_name'])->first();
        if(!$market) {
            $market = new Market([
                "name" => $params['market_name'],
                "created_by" => $request->user()->id
            ]);
            $market->save();
        }
        $price["price"] = $params["price"];
        $price["item_id"] = $item->id;
        $price["market_id"] = $market->id;
        $price["created_by"] = $request->user()->id;
        Price::create($price);

        return ApiResponse::success($item);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return ApiResponse::success(Item::findOrFail($id));
        } catch (\Throwable $th) {
            return ApiResponse::notFound("Item not found!");
        }
    }
}
