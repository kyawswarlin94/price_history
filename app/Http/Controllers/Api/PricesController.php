<?php

namespace App\Http\Controllers\Api;
// TODO: remove controller
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Price\StoreRequest;
use App\Http\Requests\Price\IndexRequest;
use Auth;
use App\Price;
use App\Http\Response\ApiResponse;

class PricesController extends Controller
{
    public function index(IndexRequest $request)
    {
        $result = isset($request->page) ? Price::orderBy('created_at', 'desc')->paginate(100) : Price::orderBy('created_at', 'desc')->get();
        $priceData = collect($result->all())->map(function ($price, $index){
            return [
                "item" => $price->item,
                "price" => $price->price,
                "market" => $price->market,
                "created_at" => $price->created_at,
                "created_by" => $price->created_by
            ];
        });
        $result->data = $priceData;
        return ApiResponse::success($result);
    }

    public function store(StoreRequest $request)
    {
        $params = $request->validated();
        $params['created_by'] = Auth::user()->id;
        return ApiResponse::success(Price::create($params));
    }
}
