<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Price;
use Illuminate\Support\Facades\DB;
use App\Item;

class HomeController extends Controller
{
    public function index(){
        return view('home');
    }
}
