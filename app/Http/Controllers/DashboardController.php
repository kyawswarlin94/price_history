<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class DashboardController extends Controller
{
    public function index(){
        return view('dashboard');
    }

    public function itemCreate(){
        return view('item.create');
    }

    public function itemList(){
        return view('item.list');
    }

    public function marketList(){
        return view('market.list');
    }
}
