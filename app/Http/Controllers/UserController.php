<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use \App\Http\ApiResponse;
use \App\User;
use Auth;

class UserController extends Controller {
	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function apiLogin(Request $request) {
		$validator = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required',
		]);

		if ($validator->fails()) {
			return ApiResponse::error($validator->errors());
		}

		$user = User::whereEmail($request->email)->first();

		if (!$user || !Hash::check($request->password, $user->password)) {
			return ApiResponse::error("email or password is wrong");
		}

		$user->rollApiKey();
		$user->save();
		return ApiResponse::success($user);
	}
/**
 * @api {get} api/access-token Get Api Access Token
 * @apiVersion 0.0.1
 * @apiName GetApiAccessToken
 * @apiGroup ApiAccessToken
 * @apiDescription GO to http://localhost:8000/api/access-token
 *
 * @apiPermission auth
 * 
 * @apiSuccess {String} $token  Api access token for loged user.
 * 
 */
	public function apiToken() {
		return Auth::user()->rollApiKey();
	}
}
