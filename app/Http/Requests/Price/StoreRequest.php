<?php

namespace App\Http\Requests\Price;

use Illuminate\Foundation\Http\FormRequest;
use App\Item;
use App\Market;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'price' => 'required|regex:/^\d*(\.\d{2})?$/',
            'item_id' => 'required|exists:items,id',
            'market_id' => 'required|exists:markets,id',
        ];
    }

    public function messages()
    {
        return [
            'price.regex' => 'Invalid price.',
            'item_id.exists' => 'Invalid Item Id.',
            'market_id.esists' => 'Invalid Market id.'
        ];
    }


}
