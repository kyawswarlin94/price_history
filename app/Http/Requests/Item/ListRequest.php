<?php

namespace App\Http\Requests\Item;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

use Illuminate\Foundation\Http\FormRequest;

class ListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'latest_price' => 'nullable',
            'market_id' => 'nullable|integer',
            'item_id' => 'nullable|integer',
        ];
    }

    protected function failedValidation(Validator $validator)
    { 
        throw new HttpResponseException(
            response()->json([
                "status"=> 422,
                "messages"=> $validator->errors()->all()
            ])
        ); 
    }
}
