<?php

namespace App\Http\Requests\Item;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Response\ApiResponse;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_name' => 'required|string|max:50',
            'sku' => 'required|string|max:50',
            'qty' => 'required|numeric|min:1',
            'unit' => 'nullable|string',
            'price' => 'required|numeric|min:1',
            'market_name' => 'required|string|max:50',
            'created_by' => 'nullable|numeric'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Item name is required!',
            'sku.required' => 'Item SKU is required!',
            'qty.required' => 'Quantity of item is required!',
            'qty.numeric' => 'Item quantity must be number',
            'qty.min' => 'Item quantity must be at least 1',
            'price.min'=> 'The price should not be 0',
            'created_by.integer' => 'Invalid User ID'
        ];
    }

    protected function failedValidation(Validator $validator)
    { 
        throw new HttpResponseException(
            response()->json([
                "status"=> 422,
                "messages"=> $validator->errors()->all()
            ])
        ); 
    }
}
