define({ "api": [
  {
    "type": "get",
    "url": "api/access-token",
    "title": "Get Api Access Token",
    "version": "0.0.1",
    "name": "GetApiAccessToken",
    "group": "ApiAccessToken",
    "description": "<p>GO to http://localhost:8000/api/access-token</p>",
    "permission": [
      {
        "name": "auth"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$token",
            "description": "<p>Api access token for loged user.</p>"
          }
        ]
      }
    },
    "filename": "../../app/Http/Controllers/UserController.php",
    "groupTitle": "ApiAccessToken"
  },
  {
    "type": "POST",
    "url": "api/items",
    "title": "Create new Item",
    "version": "0.0.1",
    "name": "Create_item_and_price",
    "group": "Item",
    "permission": [
      {
        "name": "auth:api"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer {{acces token}}",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "item_name",
            "description": "<p>The name of item</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "sku",
            "description": "<p>SKU number</p>"
          },
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": false,
            "field": "qty",
            "description": "<p>Minimum selling amunt</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "unit",
            "description": "<p>The Unit of item. Default <code>Unit</code></p>"
          },
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": false,
            "field": "price",
            "description": "<p>Selling Price</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "market_name",
            "description": "<p>The market place of selling the item.</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8000/api/items"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Objest[]",
            "optional": false,
            "field": "$result",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "Numeric",
            "optional": false,
            "field": "$result.id",
            "description": "<p>Item id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.name",
            "description": "<p>Name of the item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.sku",
            "description": "<p>SKU of the item</p>"
          },
          {
            "group": "Success 200",
            "type": "Numeric",
            "optional": false,
            "field": "$result.qty",
            "description": "<p>Quantity of minimum selling price</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.unit",
            "description": "<p>Unit of item</p>"
          },
          {
            "group": "Success 200",
            "type": "Numeric",
            "optional": false,
            "field": "$resilt.created_by",
            "description": "<p>Created user id.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "$resilt.created_at",
            "description": "<p>Created date of price.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422:InvalidParamerter",
            "description": "<p>The <code>item_id</code> must be id number.</p>"
          }
        ]
      }
    },
    "filename": "../../app/Http/Controllers/Api/ItemsController.php",
    "groupTitle": "Item"
  },
  {
    "type": "GET",
    "url": "api/items",
    "title": "Get Item List",
    "version": "0.0.1",
    "name": "GetItem",
    "group": "Item",
    "permission": [
      {
        "name": "guest"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer {{acces token}}",
            "description": ""
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8000/api/items"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Objest[]",
            "optional": false,
            "field": "$result",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.name",
            "description": "<p>Name of the item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.sku",
            "description": "<p>SKU of the item</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.qty",
            "description": "<p>Quantity of minimum selling price</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.unit",
            "description": "<p>Unit of item</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "$result.created_by",
            "description": "<p>ID of the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "$resilt.created_at",
            "description": "<p>Created date of item.</p>"
          }
        ]
      }
    },
    "filename": "../../app/Http/Controllers/Api/ItemsController.php",
    "groupTitle": "Item"
  },
  {
    "type": "GET",
    "url": "api/item/list",
    "title": "Get Item price list",
    "version": "0.0.1",
    "name": "Item_list_with_the_data_of_market_prices",
    "group": "Item",
    "permission": [
      {
        "name": "guest"
      }
    ],
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Content-Type",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Accept",
            "defaultValue": "application/json",
            "description": ""
          },
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "Authorization",
            "defaultValue": "Bearer {{acces token}}",
            "description": ""
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Boolean",
            "optional": true,
            "field": "latest_price",
            "description": "<p>To show only latest price list of items</p>"
          },
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": true,
            "field": "market_id",
            "description": "<p>To search with market id</p>"
          },
          {
            "group": "Parameter",
            "type": "Numeric",
            "optional": true,
            "field": "item_id",
            "description": "<p>To searchwith item id</p>"
          }
        ]
      }
    },
    "sampleRequest": [
      {
        "url": "http://localhost:8000/api/item/list"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Objest[]",
            "optional": false,
            "field": "$result",
            "description": ""
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.name",
            "description": "<p>Name of the item.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.sku",
            "description": "<p>SKU of the item</p>"
          },
          {
            "group": "Success 200",
            "type": "Numeric",
            "optional": false,
            "field": "$result.qty",
            "description": "<p>Quantity of minimum selling price</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.unit",
            "description": "<p>Unit of item</p>"
          },
          {
            "group": "Success 200",
            "type": "Numeric",
            "optional": false,
            "field": "$result.price",
            "description": "<p>Price of Item</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "$result.market_name",
            "description": "<p>Selling market place with  current price</p>"
          },
          {
            "group": "Success 200",
            "type": "Date",
            "optional": false,
            "field": "$resilt.price_date_at",
            "description": "<p>Created date of price.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "422:InvalidParamerter",
            "description": "<p>The <code>item_id</code> must be id number.</p>"
          }
        ]
      }
    },
    "filename": "../../app/Http/Controllers/Api/ItemsController.php",
    "groupTitle": "Item"
  }
] });
