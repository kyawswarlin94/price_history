<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', "HomeController@index");

Auth::routes();

Route::middleware('auth')->group(function () {
	Route::get("api/access-token", "UserController@apiToken");
});

Route::group(['middleware' => ['auth'], 'prefix' => 'dashboard'], function() {
	Route::get('/','DashboardController@index' )->name('dashboard');

	Route::get('/item/create','DashboardController@itemCreate' )->name('dashboard.itemCreate');
	Route::get('/items','DashboardController@itemList' )->name('dashboard.itemList');
	Route::get('/markets','DashboardController@marketList' )->name('dashboard.marketList');
});