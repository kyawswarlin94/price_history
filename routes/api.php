<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
	return $request->user();
});

Route::post("user/login", "UserController@apiLogin");

Route::group(['namespace' => 'Api'], function () {
	Route::group(['middleware'=> ['auth:api']], function() {
		Route::resource('prices', 'PricesController')->only([
			'index', 'store'
		]);
		Route::resource('markets', 'MarketsController');
		Route::post('items', 'ItemsController@store');
	});
	Route::get('item/list', 'ItemsController@list')->name('items.list');

	Route::get('item/list', 'ItemsController@list')->name('items.list');

	Route::get('markets', 'MarketsController@index')->name('markets');

	Route::get('items', 'ItemsController@index')->name('items');
});