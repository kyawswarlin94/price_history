<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- plugins:css -->
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/mdi/css/materialdesignicons.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/ionicons/css/ionicons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/typicons/src/font/typicons.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/iconfonts/flag-icon-css/css/flag-icon.min.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('css/shared/style.css')}}">
    <!-- endinject -->
    <!-- Layout styles -->
    <link rel="stylesheet" href="{{asset('css/demo_1/style.css')}}">
    <!-- End Layout styles -->
    <link rel="shortcut icon" href="{{asset('images/favicon.png')}}"/>
  </head>
  <body>
    <div id="app" class="container-scroller">
      <!-- partial:partials/_navbar.html -->
      @yield('navbar')
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
            <ul class="nav">
                <li class="nav-item nav-profile">
                    <a href="#" class="nav-link">
                        <div class="profile-image">
                            <img class="img-xs rounded-circle" src="https://scontent.frgn3-1.fna.fbcdn.net/v/t1.0-0/p370x247/20246117_1377317095670919_5051654893251928929_n.jpg?_nc_cat=107&_nc_sid=85a577&_nc_eui2=AeH0wvl8vbzEKz5FmlAYF8D0WOQgT0Z9__hY5CBPRn3_-LwyBPWw2ea9YcxxhrjrilhY1FxsEJvvPi0LCLkmMrUR&_nc_ohc=xb3IuQzecDEAX_7AVbg&_nc_ht=scontent.frgn3-1.fna&_nc_tp=6&oh=c93c4f21cc8ad3c5250f7b9989a8f748&oe=5F0BC5D8" alt="profile image">
                            <div class="dot-indicator bg-success"></div>
                        </div>
                        <div class="text-wrapper">
                          <p class="profile-name">
                            Kyaw Swar Linn
                          </p>
                          <p class="designation">Web Developer</p>
                        </div>
                    </a>
                </li>
                <li class="nav-item nav-category">Admin Page</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard') }}">
                      <i class="menu-icon typcn typcn-document-text"></i>
                      <span class="menu-title">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="{{ route('dashboard.marketList') }}">
                    <i class="menu-icon typcn typcn-document-text"></i>
                    <span class="menu-title">Market</span>
                  </a>
                </li>
                <li class="nav-item" >
                  <a class="nav-link" data-toggle="collapse" href="#items" aria-expanded="false" aria-controls="items">
                    <i class="menu-icon typcn typcn-coffee"></i>
                      <span class="menu-title">Item</span>
                    <i class="menu-arrow"></i>
                  </a>
                  <div data-toggle="collapse" id="#items-create">
                    <ul class="nav flex-column sub-menu">
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.itemList') }}" href="pages/ui-features/dropdowns.html">List Item</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.itemCreate') }}">Create Item</a>
                      </li>
                    </ul>
                  </div>
                </li>
            </ul>
        </nav>
        <!-- partial -->
        @yield('content')
        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->
    <!-- plugins:js -->
    <script src="{{ asset('vendors/js/vendor.bundle.base.js') }}"></script>
    <script src="{{ asset('vendors/js/vendor.bundle.addons.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('js/shared/off-canvas.js')}}"></script>
    <script src="{{ asset('js/shared/misc.js')}}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('js/demo_1/dashboard.js')}}"></script>
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
            'apiToken' => Auth::user()->rollApiKey()['token'] ?? null,
        ]) !!};
     </script>
    <!-- End custom js for this page-->
  </body>
</html>