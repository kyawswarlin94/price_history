@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div id="app" class="col-lg-12">
            <item-list></item-list>
        </div>
    </div>
</div>
@endsection
