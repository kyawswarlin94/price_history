require('./bootstrap');

window.Vue = require('vue');
axios.defaults.headers.common = {
  'X-CSRF-TOKEN': Laravel.csrfToken,
  'X-Requested-With': 'XMLHttpRequest',
  'Authorization': 'Bearer ' + Laravel.apiToken,
};

import ItemList from './components/itemList.vue'
import ItemCreate from './components/itemCreate.vue'
import Items from './components/items.vue'
import Markets from './components/markets.vue'
import { BootstrapVue } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)

const app = new Vue({
    el: '#app',
    components: {
      ItemList,
      ItemCreate,
      Items,
      Markets
    }
});
