<img src="https://kyawswarlinn.000webhostapp.com/price_history.png"/>

### Requirement
- php v7.2
- node v12
- mysql

### Installation
- Set Database configuration in .env file

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=price_history
DB_USERNAME=root
DB_PASSWORD=secret
```

- $ cd path_to_project_directory
- $ composer install
- $ php artisan key:generate
- $ npm install
- $ php artisan migrate --seed

### Start the server
- $ php artisan serve


### API Doc
Open `path_to_project_directory/doc/api/src/index.html`



### Member account
```
email: badin@example.com
password: secret
```
<img src="https://kyawswarlinn.000webhostapp.com/price-history-dash.png"/>

